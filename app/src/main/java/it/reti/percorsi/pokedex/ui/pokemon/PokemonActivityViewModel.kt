package it.reti.percorsi.pokedex.ui.pokemon

import android.app.Activity
import android.app.Application
import android.content.Context
import android.media.Image
import android.util.Log
import androidx.lifecycle.*
import io.reactivex.subscribers.DisposableSubscriber
import it.reti.percorsi.pokedex.data.PokemonDatabase
import it.reti.percorsi.pokedex.entities.Pokemon
import it.reti.percorsi.pokedex.network.PokemonApi
import it.reti.percorsi.pokedex.utils.LocalRepository
import it.reti.percorsi.pokedex.utils.MockRepository
import it.reti.percorsi.pokedex.utils.RemoteRepository
import kotlinx.coroutines.Dispatchers

class PokemonActivityViewModel(application: Activity, id: String) : ViewModel() {

    //Repository refs
    private var localRepository: LocalRepository
    private var remoteRepository: RemoteRepository

    //Observable vars
    var pokemon: MutableLiveData<Pokemon> = MutableLiveData()
    private val pokemonId = MutableLiveData<String>(id)

    init {
        localRepository = LocalRepository(PokemonDatabase.getDatabase(application))
        remoteRepository = RemoteRepository(PokemonApi.create())
    }

    fun getPokemonById() {
        remoteRepository.getPokemonById(Integer.parseInt(pokemonId.value!!))
            .subscribe(object : DisposableSubscriber<Pokemon>() {
                override fun onStart() {
                    super.onStart()
                }

                override fun onComplete() {
                    //not needed
                }

                override fun onNext(t: Pokemon?) {
                    pokemon.value = t
                }

                override fun onError(t: Throwable?) {
                    Log.e("PkmActivityViewModel", t!!.message)
                }
            })
    }
}

@Suppress("UNCHECKED_CAST")
class PokemonActivityViewModelFactory(private val application: Activity, private val pokemonId: String) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PokemonActivityViewModel(application, pokemonId) as T
    }
}