package it.reti.percorsi.pokedex.utils

import android.view.View
import android.widget.ImageView
import androidx.core.view.isGone
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

fun <L : Any, R : Any> Iterable<L>.joinBy(
    collection: Iterable<R>,
    filter: (L, R) -> Boolean
): Iterable<Pair<L, R>> =
    flatMap { l ->

        collection.filter { r ->
            filter.invoke(l, r)

        }.map { r ->
            Pair(l, r)
        }
    }


@set:BindingAdapter("visible")
var View.visible
    get() = isVisible
    set(value) {
        isVisible = value
    }

@set:BindingAdapter("invisible")
var View.invisible
    get() = isInvisible
    set(value) {
        isInvisible = value
    }

@set:BindingAdapter("gone")
var View.gone
    get() = isGone
    set(value) {
        isGone = value
    }


@BindingAdapter("imageUrl")
fun ImageView.setImageUrl(url: String?) {
    Glide.with(context).load(url).into(this)
}