package it.reti.percorsi.pokedex.entities

class Pokemon(
    val id: String,
    val tag: String,
    val url: String?,
    val typeList: MutableList<PokemonType>?
) {

    constructor(): this ("", "", "", mutableListOf()){

    }

    constructor(
        id: String,
        name: String,
        url: String,
        typeList: MutableList<PokemonType>?,
        evolutionId: String
    ) : this(id, name, url, typeList) {
        _evolution = Pokemon(evolutionId, "", "", ArrayList())
    }

    var imageUrl: String =
        "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/$id.png"

    val name: String = tag.capitalize()

    private var _evolution: Pokemon? = null
    val evolutionId: String?
        get() {
            return _evolution?.id
        }

    var evolution: Pokemon?
        get() {
            return if (_evolution?.typeList?.any() == true) _evolution else null
        }
        set(value) {
            _evolution = value
        }


    override fun toString(): String = tag
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Pokemon) return false

        if (id != other.id) return false
        if (tag != other.tag) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + tag.hashCode()
        return result
    }
}