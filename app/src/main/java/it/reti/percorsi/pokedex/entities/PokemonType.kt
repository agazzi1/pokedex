package it.reti.percorsi.pokedex.entities

data class PokemonType(val pType: PType) {

    val tag = pType.key
    val name = tag.capitalize()
    val pokemonList = ArrayList<Pokemon>()

    override fun toString(): String = tag

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is PokemonType) return false

        if (pType != other.pType) return false

        return true
    }


    override fun hashCode(): Int {
        return pType.hashCode()
    }

    companion object {
        fun all() = PType.values().map { PokemonType(it) }
    }
}