package it.reti.percorsi.pokedex.ui.pokemonList

import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.work.*
import io.reactivex.Observable
import it.reti.percorsi.pokedex.R
import it.reti.percorsi.pokedex.entities.PType
import it.reti.percorsi.pokedex.entities.Pokemon
import it.reti.percorsi.pokedex.entities.PokemonType
import it.reti.percorsi.pokedex.synchro.SynchroWorker
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [PokemonListFragment.OnListFragmentInteractionListener] interface.
 */
class PokemonListFragment : Fragment() {

    private var columnCount = 1
    private var pokemonTypes = listOf<PokemonType>()
    private var listener: OnListFragmentInteractionListener? = null
    private lateinit var viewModel: PokemonListFragmentViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView: View = inflater.inflate(R.layout.fragment_pokemon_list, container, false)

        rootView.findViewById<Button>(R.id.synchBtn).setOnClickListener(View.OnClickListener {
            val synchroRequest = PeriodicWorkRequestBuilder<SynchroWorker>(30, TimeUnit.SECONDS)
                .build()
            WorkManager.getInstance(context!!).enqueue(synchroRequest)
        })

        rootView.findViewById<Button>(R.id.refreshBtn).setOnClickListener(View.OnClickListener {
            viewModel.getLocalPokemonList()
        })

        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT, columnCount)
            val parameter = PokemonListFragmentArgs.fromBundle(it).pokemonType
            pokemonTypes = PokemonType.all()
                .filter { pType -> pType.tag == parameter }
                .ifEmpty { PokemonType.all() }
        } ?: run {
            pokemonTypes = PokemonType.all()
        }

        viewModel = ViewModelProvider(
            this,
            PokemonListViewModelFactory(requireActivity(), pokemonTypes.toTypedArray())
        ).get(PokemonListFragmentViewModel::class.java)

        viewModel.pokemonList.observe(viewLifecycleOwner, Observer {
            updateViews(it)
        })
    }

    private fun updateViews(pokemonList: List<Pokemon>) {
        // Set the adapter
        val v = view?.findViewById<RecyclerView>(R.id.fragment_pokemon_list_recycler)
        if (v is RecyclerView) {
            with(v) {
                layoutManager = layoutManager ?: when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                adapter = adapter ?: PokemonListAdapter(
                    listener
                )
                (adapter as PokemonListAdapter).submitList(pokemonList)
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnListFragmentInteractionListener")
        }
    }

    //ASK WHY THIS DOESN'T MANTAIN DATA
    override fun onResume() {
        super.onResume()
        updateViews(viewModel.pokemonList.value?: listOf())
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.fragment_pokemon_list_menu, menu);
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.fragment_pokemon_list_menu_dark -> {
                viewModel.pokemonTypes.value = arrayOf(PokemonType(PType.DARK))
            }

            R.id.fragment_pokemon_list_menu_steel -> {
                viewModel.pokemonTypes.value = arrayOf(PokemonType(PType.STEEL))
            }
        }

        return true
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: Pokemon)
    }

    companion object {

        private const val ARG_COLUMN_COUNT = "column-count"
        private const val ARG_PARAMETER_TYPE = "pokemon-type"

        fun newInstance() =
            newInstance(1, null)

        fun newInstance(columnCount: Int, pokemonType: PokemonType?) =
            PokemonListFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                    putString(ARG_PARAMETER_TYPE, pokemonType?.tag)
                }
            }
    }
}
