package it.reti.percorsi.pokedex.network

import android.media.Image
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.reactivex.Flowable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PokemonApi {

    @GET("pokemon")
    fun pokemonById(@Query("id") id: Int): Flowable<SinglePokemonModelResponse>

    @GET("pokemon")
    fun listOfPokemon(@Query("limit") limit: Int, @Query("offset") offset: Int): Flowable<PokemonModelObjectResponse>

    companion object {
        fun create(): PokemonApi {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl("http://pokeapi.co/api/v1/")
                .build()

            return retrofit.create(PokemonApi::class.java)
        }

        fun getGson(): Gson {
            return GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()
        }
    }

}