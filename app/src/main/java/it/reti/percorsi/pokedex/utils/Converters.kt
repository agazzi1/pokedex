package it.reti.percorsi.pokedex.utils

import androidx.room.TypeConverter
import com.google.gson.Gson
import it.reti.percorsi.pokedex.entities.PokemonType

class Converters {
    @TypeConverter
    fun listToJson(value: List<PokemonType>?): String {
        return Gson().toJson(value)
    }

    @TypeConverter
    fun jsonToList(value: String): List<PokemonType>? {
        val objects = Gson().fromJson(value, Array<PokemonType>::class.java) as Array<PokemonType>
        val list = objects.toList()
        return list
    }
}