package it.reti.percorsi.pokedex.ui.pokemonList


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import it.reti.percorsi.pokedex.R
import it.reti.percorsi.pokedex.entities.Pokemon
import it.reti.percorsi.pokedex.ui.pokemonList.PokemonListFragment.OnListFragmentInteractionListener
import kotlinx.android.synthetic.main.item_pokemon.view.*


/**
 * [RecyclerView.Adapter] that can display a [Pokemon] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 */
class PokemonListAdapter(
    private val listener: OnListFragmentInteractionListener?
) : ListAdapter<Pokemon, PokemonListAdapter.ViewHolder>(PokemonDiffCallback()) {

    private val onClickListener: View.OnClickListener

    init {
        onClickListener = View.OnClickListener { v ->
            val item = v.tag as Pokemon
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            listener?.onListFragmentInteraction(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_pokemon, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.idView.text = item.id
        holder.nameView.text = item.name

        with(holder.mView) {
            tag = item
            setOnClickListener(onClickListener)
        }
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val idView: TextView = mView.item_pokemon_id
        val nameView: TextView = mView.item_pokemon_name

        override fun toString(): String {
            return super.toString() + " '" + nameView.text + "'"
        }
    }

    class PokemonDiffCallback : DiffUtil.ItemCallback<Pokemon>() {
        override fun areItemsTheSame(oldItem: Pokemon, newItem: Pokemon): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Pokemon, newItem: Pokemon): Boolean {
            return oldItem == newItem
        }

    }
}
