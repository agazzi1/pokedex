package it.reti.percorsi.pokedex.synchro

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.asLiveData
import androidx.work.CoroutineWorker
import androidx.work.Worker
import androidx.work.WorkerParameters
import io.reactivex.schedulers.Schedulers
import io.reactivex.subscribers.DisposableSubscriber
import it.reti.percorsi.pokedex.data.PokemonDatabase
import it.reti.percorsi.pokedex.entities.Pokemon
import it.reti.percorsi.pokedex.network.PokemonApi
import it.reti.percorsi.pokedex.utils.LocalRepository
import it.reti.percorsi.pokedex.utils.RemoteRepository

class SynchroWorker(appContext: Context, workerParams: WorkerParameters)
    : Worker(appContext, workerParams) {

    val TAG: String = "SynchroWorker"

    override fun doWork(): Result {

        val parameters: List<Int> = RandomAlghoritm.getRandomLimitAndOffset()

        RemoteRepository(PokemonApi.create()).getAllPokemon(parameters[0], parameters[1])
            .subscribe(object : DisposableSubscriber<List<Pokemon>>() {
                override fun onStart() {
                    super.onStart()
                    Log.d(TAG, "Job is starting at " + System.currentTimeMillis())
                }

                override fun onComplete() {
                    Log.d(TAG, "Job has completed syncronization at " + System.currentTimeMillis())
                }

                override fun onNext(t: List<Pokemon>?) {
                    for (item in t!!){
                        LocalRepository(PokemonDatabase.getDatabase(applicationContext))
                            .insertPokemon(item)
                    }
                }

                override fun onError(t: Throwable?) {
                    Log.e(TAG, t!!.message)
                }
        })

        // Indicate whether the task finished successfully with the Result
        return Result.success()

    }
}