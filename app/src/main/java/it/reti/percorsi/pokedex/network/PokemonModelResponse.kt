package it.reti.percorsi.pokedex.network

import com.google.gson.annotations.Expose
import it.reti.percorsi.pokedex.entities.Pokemon

data class PokemonModelResponse(
    @Expose
    val name: String,
    @Expose
    val url: String
)
data class PokemonModelObjectResponse(
    @Expose
    val count: Int,
    @Expose
    val next: String,
    @Expose
    val previous: String?,
    @Expose
    val results: List<PokemonModelResponse>
) {
    fun mapToPokemonList(): List<Pokemon> {
        val pokemonList: MutableList<Pokemon> = mutableListOf()
        for (item in results){
            pokemonList.add(
                Pokemon(
                    //"https://pokeapi.co/api/v2/pokemon/1/"
                    item.url.replace("https://pokeapi.co/api/v2/pokemon/", "").replace("/", ""),
                    item.name,
                    item.url,
                    mutableListOf()
                )
            )
        }
        return pokemonList
    }
}