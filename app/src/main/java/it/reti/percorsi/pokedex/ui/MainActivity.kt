package it.reti.percorsi.pokedex.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import it.reti.percorsi.pokedex.R
import it.reti.percorsi.pokedex.entities.Pokemon
import it.reti.percorsi.pokedex.entities.PokemonType
import it.reti.percorsi.pokedex.ui.pokemon.PokemonActivity
import it.reti.percorsi.pokedex.ui.pokemonList.PokemonListFragment
import it.reti.percorsi.pokedex.ui.pokemonTypeList.PokemonTypeListFragment
import it.reti.percorsi.pokedex.ui.pokemonTypeList.PokemonTypeListFragmentDirections

class MainActivity : AppCompatActivity(),
    PokemonTypeListFragment.OnListFragmentInteractionListener,
    PokemonListFragment.OnListFragmentInteractionListener {

    override fun onListFragmentInteraction(item: Pokemon) {
        PokemonActivity.start(this, item.id)
    }

    override fun onListFragmentInteraction(item: PokemonType) {
        //Make some type of interaction
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_pokemon_type_list,
                R.id.nav_pokemon_list
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }
}
