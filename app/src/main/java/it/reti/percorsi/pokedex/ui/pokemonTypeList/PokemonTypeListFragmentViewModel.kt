package it.reti.percorsi.pokedex.ui.pokemonTypeList

import androidx.lifecycle.*
import it.reti.percorsi.pokedex.entities.PokemonType
import it.reti.percorsi.pokedex.utils.MockRepository
import kotlinx.coroutines.Dispatchers

class PokemonTypeListFragmentViewModel : ViewModel() {

    private lateinit var pokemonTypeList: LiveData<List<PokemonType>>

    fun getPokemonTypeList(): LiveData<List<PokemonType>> {
        if (!::pokemonTypeList.isInitialized) {
            pokemonTypeList = MockRepository.getPokemonTypeListFlow().asLiveData(
                viewModelScope.coroutineContext + Dispatchers.IO,
                300L
            )
                .map { sort(it) }
        }

        return pokemonTypeList
    }

    private fun sort(list: Collection<PokemonType>) =
        list.sortedBy { it.tag }
}
