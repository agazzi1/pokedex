package it.reti.percorsi.pokedex.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import com.google.gson.Gson
import it.reti.percorsi.pokedex.entities.PType
import it.reti.percorsi.pokedex.entities.Pokemon
import it.reti.percorsi.pokedex.entities.PokemonType

@Entity
data class PokemonDto (
    @PrimaryKey val uid: String,
    @ColumnInfo val description: String,
    @ColumnInfo val tag: String,
    @ColumnInfo val typeList: List<PokemonType>,
    @ColumnInfo val evolutionId: String
){
    fun toPokemon(): Pokemon {
        return Pokemon(uid, tag, "", mutableListOf(), evolutionId)
    }
}


