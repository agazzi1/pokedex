package it.reti.percorsi.pokedex.utils

import android.content.Context
import android.content.res.ColorStateList
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.appcompat.widget.AppCompatButton
import androidx.databinding.BindingAdapter
import it.reti.percorsi.pokedex.R
import it.reti.percorsi.pokedex.entities.PokemonType


fun getPokemonType(pokemonTypes: List<PokemonType>): String =
    pokemonTypes.map { it.name }.reduce { a, b -> "$a | $b" }

@ColorInt
fun getEvolutionBackgroundColor(context: Context, exist: Boolean): Int {
    val colorResource = getEvolutionBackgroundColorResource(exist)
    return context.getColor(colorResource)
}

@ColorRes
fun getEvolutionBackgroundColorResource(exist: Boolean): Int {
    return if (exist) R.color.colorPrimary else R.color.md_amber_200
}


@BindingAdapter("backgroundColor")
fun setBackgroundColor(button: AppCompatButton, @ColorRes color: Int) {
    button.backgroundTintList = ColorStateList.valueOf(button.context.getColor(color))
}

