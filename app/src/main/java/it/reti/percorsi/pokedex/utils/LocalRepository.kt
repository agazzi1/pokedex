package it.reti.percorsi.pokedex.utils

import android.util.Log
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.schedulers.Schedulers
import it.reti.percorsi.pokedex.data.PokemonDatabase
import it.reti.percorsi.pokedex.data.PokemonDto
import it.reti.percorsi.pokedex.entities.Pokemon

class LocalRepository (
    private val db: PokemonDatabase
){
    fun insertPokemon(pkm: Pokemon): DisposableCompletableObserver {
        return db.pokemonDAO().insertPokemon(
            PokemonDto(
            pkm.id,
            pkm.name,
            pkm.tag,
            listOf(),
            pkm.evolutionId?:""
            )
        )
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object: DisposableCompletableObserver() {
                override fun onStart() {

                }
                override fun onError(error: Throwable) {
                    Log.d("TAG", error.message)
                }
                override fun onComplete() {
                    Log.d("TAG", "")
                }
            });
    }

    fun getAllPokemon(): Flowable<List<Pokemon>> {
        return db.pokemonDAO().getAll()
            .subscribeOn(Schedulers.io())
            .map{
                mapList(it)
            }
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun mapList(dtos: List<PokemonDto>): List<Pokemon> {
        val pkmList: MutableList<Pokemon> = mutableListOf()
        for (item in dtos){
            pkmList.add(item.toPokemon())
        }
        return pkmList
    }

}