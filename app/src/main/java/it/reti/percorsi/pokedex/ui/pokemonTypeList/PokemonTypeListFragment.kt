package it.reti.percorsi.pokedex.ui.pokemonTypeList

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import it.reti.percorsi.pokedex.R
import it.reti.percorsi.pokedex.entities.PokemonType

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [PokemonTypeListFragment.OnListFragmentInteractionListener] interface.
 */
class PokemonTypeListFragment : Fragment() {

    private var columnCount = 1

    private var listener: OnListFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_pokemon_type_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val viewModel = ViewModelProvider(this).get(PokemonTypeListFragmentViewModel::class.java)

        //CALLED WITH COROUTINES
        viewModel.getPokemonTypeList().observe(viewLifecycleOwner, Observer {
            updateViews(it)
        })

    }

    private fun updateViews(pokemonTypeList: List<PokemonType>) {
        // Set the adapter
        val v = view
        if (v is RecyclerView) {
            with(v) {
                layoutManager = layoutManager ?: when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                adapter = adapter ?: PokemonTypeListAdapter(
                    listener
                )
                (adapter as PokemonTypeListAdapter).submitList(pokemonTypeList)
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: PokemonType)
    }

    companion object {
        private const val ARG_COLUMN_COUNT = "column-count"
        @JvmStatic
        fun newInstance(columnCount: Int) =
            PokemonTypeListFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}
