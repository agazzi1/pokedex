package it.reti.percorsi.pokedex.ui.pokemonTypeList


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import it.reti.percorsi.pokedex.R
import it.reti.percorsi.pokedex.entities.PokemonType
import it.reti.percorsi.pokedex.ui.pokemonTypeList.PokemonTypeListFragment.OnListFragmentInteractionListener
import it.reti.percorsi.pokedex.utils.MockRepository
import kotlinx.android.synthetic.main.item_pokemon_type.view.*

/**
 * [RecyclerView.Adapter] that can display a [PokemonType] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 */
class PokemonTypeListAdapter(
    private val listener: OnListFragmentInteractionListener?
) : ListAdapter<PokemonType, PokemonTypeListAdapter.ViewHolder>(
    PokemonTypeDiffCallback()
) {

    private val onClickListener: View.OnClickListener

    init {
        onClickListener = View.OnClickListener { v ->
            val item = v.tag as PokemonType
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            listener?.onListFragmentInteraction(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_pokemon_type, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.typeName.text = item.name
        holder.typeIcon.setImageDrawable(
            MockRepository.getTypeIcon(
                item.pType,
                holder.typeIcon.context
            )
        )

        with(holder.mView) {
            tag = item
            setOnClickListener(onClickListener)
        }
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val typeIcon: ImageView = mView.item_pokemon_type_icon
        val typeName: TextView = mView.item_pokemon_type_name

        override fun toString(): String {
            return super.toString() + " '" + typeName.text + "'"
        }
    }

    class PokemonTypeDiffCallback : DiffUtil.ItemCallback<PokemonType>() {
        override fun areItemsTheSame(oldItem: PokemonType, newItem: PokemonType): Boolean {
            return oldItem.tag == newItem.tag
        }

        override fun areContentsTheSame(oldItem: PokemonType, newItem: PokemonType): Boolean {
            return oldItem == newItem
        }

    }
}
