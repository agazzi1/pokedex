package it.reti.percorsi.pokedex.utils

import android.media.Image
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import it.reti.percorsi.pokedex.entities.Pokemon
import it.reti.percorsi.pokedex.network.PokemonApi
import it.reti.percorsi.pokedex.network.PokemonModelObjectResponse

class RemoteRepository (
    private val api: PokemonApi
){
    fun getPokemonById(pokemonId: Int): Flowable<Pokemon> {
        return api.pokemonById(pokemonId)
            .subscribeOn(Schedulers.io())
            .map {
                it.mapToPokemon()
            }
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun getAllPokemon(limit: Int, offset: Int): Flowable<List<Pokemon>> {
        return api.listOfPokemon(limit, offset)
            .subscribeOn(Schedulers.io())
            .map{
                it.mapToPokemonList()
            }
            .observeOn(AndroidSchedulers.mainThread())
    }

}