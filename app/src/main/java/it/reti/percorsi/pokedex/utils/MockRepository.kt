package it.reti.percorsi.pokedex.utils

import android.content.Context
import android.graphics.drawable.Drawable
import android.graphics.drawable.PaintDrawable
import it.reti.percorsi.pokedex.R
import it.reti.percorsi.pokedex.entities.PType
import it.reti.percorsi.pokedex.entities.Pokemon
import it.reti.percorsi.pokedex.entities.PokemonType
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.util.*

object MockRepository {

    fun getPokemonTypeListFlow(): Flow<Collection<PokemonType>> = flow {
        emit(pokemonTypeMap.values)
    }

    fun getPokemonListFlow(pokemonType: Array<PokemonType>): Flow<Collection<Pokemon>> = flow {
        pokemonMap.forEach { entry -> pokemonMap[entry.key] = getPokemon(entry.key) }

        emit(pokemonMap.filter { pokemonEntry ->
            pokemonEntry.value.typeList!!.any { t ->
                pokemonType.map { pt -> pt.tag }.contains(t.tag)
            } &&
                    pokemonMap.values.none { prev -> prev.evolution?.id == pokemonEntry.value.id } // non è evoluzione
        }.values)
    }

    fun getPokemonFlow(pokemonId: String): Flow<Pokemon> = flow {
        val pokemon = getPokemon(pokemonId)
        emit(pokemon)
    }

    private fun getPokemon(pokemonId: String): Pokemon {
        val pokemon = pokemonMap[pokemonId]!!
        pokemonMap.values.firstOrNull { it.id == pokemon.evolutionId }
            ?.let { pokemon.evolution = it }

        return pokemon
    }

    private val pokemonTypeMap: MutableMap<String, PokemonType> = HashMap()
    private val pokemonMap: MutableMap<String, Pokemon> = HashMap()
    private const val COUNT = 25


    init {
        val normal = PokemonType(PType.NORMAL)
        addItem(normal)
        val fighting = PokemonType(PType.FIGHTING)
        addItem(fighting)
        val flynig = PokemonType(PType.FLYING)
        addItem(flynig)
        val poison = PokemonType(PType.POISON)
        addItem(poison)
        val ground = PokemonType(PType.GROUND)
        addItem(ground)
        val rock = PokemonType(PType.ROCK)
        addItem(rock)
        val bug = PokemonType(PType.BUG)
        addItem(bug)
        val ghost = PokemonType(PType.GHOST)
        addItem(ghost)
        val steel = PokemonType(PType.STEEL)
        addItem(steel)
        val fire = PokemonType(PType.FIRE)
        addItem(fire)
        val water = PokemonType(PType.WATER)
        addItem(water)
        val grass = PokemonType(PType.GRASS)
        addItem(grass)
        val electric = PokemonType(PType.ELECTRIC)
        addItem(electric)
        val psychic = PokemonType(PType.PSYCHIC)
        addItem(psychic)
        val ice = PokemonType(PType.ICE)
        addItem(ice)
        val dragon = PokemonType(PType.DRAGON)
        addItem(dragon)
        val dark = PokemonType(PType.DARK)
        addItem(dark)
        val fairy = PokemonType(PType.FAIRY)
        addItem(fairy)
        val unknown = PokemonType(PType.UNKNOWN)
        addItem(unknown)

        addItem(Pokemon(702.toString(), "dedenne", "", mutableListOf(fairy, electric)))
        addItem(Pokemon(718.toString(), "zygarde", "", mutableListOf(ground, dragon)))
        addItem(Pokemon(221.toString(), "piloswine", "", mutableListOf(ground, ice), 473.toString()))
        addItem(Pokemon(473.toString(), "mamoswine", "", mutableListOf(ground, ice)))
        addItem(Pokemon(447.toString(), "riolu", "", mutableListOf(fighting), 448.toString()))
        addItem(Pokemon(448.toString(), "lucario",  "",mutableListOf(steel, fighting)))
        addItem(Pokemon(559.toString(), "scraggy", "", mutableListOf(fighting, dark), 560.toString()))
        addItem(Pokemon(560.toString(), "scrafty",  "",mutableListOf(fighting, dark)))
        addItem(Pokemon(379.toString(), "registeel",  "",mutableListOf(steel)))
        addItem(Pokemon(446.toString(), "munchlax", "", mutableListOf(normal), 143.toString()))
        addItem(Pokemon(143.toString(), "snorlax", "", mutableListOf(normal)))
        addItem(Pokemon(108.toString(), "lickitung", "", mutableListOf(normal)))
        addItem(Pokemon(166.toString(), "ledian",  "",mutableListOf(flynig, bug)))
        addItem(Pokemon(338.toString(), "solrock", "", mutableListOf(psychic, rock)))
        addItem(Pokemon(83.toString(), "farfetchd", "", mutableListOf(normal)))
        addItem(Pokemon(488.toString(), "cresselia", "", mutableListOf(psychic)))

    }

    private fun addItem(item: Pokemon) {
        pokemonMap[item.id] = item
    }

    private fun addItem(item: PokemonType) {
        pokemonTypeMap[item.name] = item
    }

    fun getTypeIcon(type: PType, context: Context): Drawable {
        val icon = when (type) {
            PType.BUG -> R.drawable.bug
            PType.DARK -> R.drawable.dark
            PType.DRAGON -> R.drawable.dragon
            PType.ELECTRIC -> R.drawable.electric
            PType.FAIRY -> R.drawable.fairy
            PType.FIGHTING -> R.drawable.fighting
            PType.FIRE -> R.drawable.fire
            PType.FLYING -> R.drawable.flying
            PType.GHOST -> R.drawable.ghost
            PType.GRASS -> R.drawable.grass
            PType.GROUND -> R.drawable.ground
            PType.ICE -> R.drawable.ice
            PType.NORMAL -> R.drawable.normal
            PType.POISON -> R.drawable.poison
            PType.PSYCHIC -> R.drawable.psychic
            PType.ROCK -> R.drawable.rock
            PType.STEEL -> R.drawable.steel
            PType.WATER -> R.drawable.water

            else -> -1
        }

        val drawable = PaintDrawable()

        return if (icon != -1) context.getDrawable(icon)!! else drawable
    }
}
