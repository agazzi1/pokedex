package it.reti.percorsi.pokedex.ui.pokemon

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import it.reti.percorsi.pokedex.R
import it.reti.percorsi.pokedex.databinding.ActivityPokemonBinding
import it.reti.percorsi.pokedex.entities.Pokemon

class PokemonActivity : AppCompatActivity() {

    private var pokemonId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        pokemonId = intent.getStringExtra(POKEMON_ID)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val viewModel = ViewModelProvider(
            this,
            PokemonActivityViewModelFactory(this, pokemonId)
        ).get(PokemonActivityViewModel::class.java)

        val contentView = DataBindingUtil.setContentView<ActivityPokemonBinding>(
            this,
            R.layout.activity_pokemon
        )

        //See data of the selected pokemon
        contentView.pokemon = Pokemon()
        contentView.pokemon?.imageUrl = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/$pokemonId.png"
        contentView.vm = viewModel

        //If room download real pokemon
        viewModel.pokemon.observe(this, Observer {

            val contentView = DataBindingUtil.setContentView<ActivityPokemonBinding>(
                this,
                R.layout.activity_pokemon
            )

            //This is to show the pokemon
            supportActionBar?.title = it.name
            contentView.pokemon = it
            contentView.vm = viewModel

        })

        viewModel.getPokemonById()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }


    companion object {
        private const val POKEMON_ID = "pokemon-id"

        fun start(context: Context, pokemonId: String) {
            val intent = Intent(context, PokemonActivity::class.java)
            intent.putExtra(POKEMON_ID, pokemonId)

            context.startActivity(intent)
        }
    }
}
