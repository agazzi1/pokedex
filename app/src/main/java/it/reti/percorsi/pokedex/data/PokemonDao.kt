package it.reti.percorsi.pokedex.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Flowable
import org.intellij.lang.annotations.Flow

@Dao
interface PokemonDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPokemon(pokemon: PokemonDto): Completable

    @Query("SELECT * FROM PokemonDto")
    fun getAll(): Flowable<List<PokemonDto>>

    @Query("SELECT * FROM PokemonDto WHERE uid = :uid")
    fun getPokemonById(uid: Int): LiveData<PokemonDto>
}