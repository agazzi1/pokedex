package it.reti.percorsi.pokedex.network

import com.google.gson.annotations.Expose
import it.reti.percorsi.pokedex.entities.PType
import it.reti.percorsi.pokedex.entities.Pokemon
import it.reti.percorsi.pokedex.entities.PokemonType

data class SinglePokemonModelResponse(
    @Expose val id: Int,
    @Expose val name: String,
    val base_experience: Int,
    val height: Int,
    val is_default: Boolean,
    val order: Boolean,
    val weight: Int,
    val abilities: String? = null,
    val forms: String? = null,
    val game_indices: String? = null,
    val held_items: String? = null,
    val location_area_encounters: String? = null,
    val moves: String? = null,
    val sprites: String? = null,
    val species: String? = null,
    val stats: String? = null,
    @Expose val types: List<PokemonTypeModelOuterResponse>
){
    fun mapToPokemon(): Pokemon {
        val pokemon = Pokemon()
        for (item in types){
            val typeList: List<PokemonTypeModelInnerResponse> = item.type
            for (type in typeList){
                pokemon.typeList!!.add(PokemonType(
                    PType.valueOf(type.name)
                ))
            }
        }
        return pokemon
    }
}

data class PokemonTypeModelOuterResponse (
    @Expose val slot: Int,
    @Expose val type: List<PokemonTypeModelInnerResponse>
)

data class PokemonTypeModelInnerResponse(
    @Expose val name: String,
    @Expose val url: String
)