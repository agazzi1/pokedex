package it.reti.percorsi.pokedex.ui.pokemonList

import android.app.Activity
import android.app.Application
import android.database.Observable
import androidx.lifecycle.*
import androidx.work.impl.Schedulers
import io.reactivex.subscribers.DisposableSubscriber
import it.reti.percorsi.pokedex.data.PokemonDatabase
import it.reti.percorsi.pokedex.entities.PType
import it.reti.percorsi.pokedex.entities.Pokemon
import it.reti.percorsi.pokedex.entities.PokemonType
import it.reti.percorsi.pokedex.network.PokemonApi
import it.reti.percorsi.pokedex.network.PokemonModelObjectResponse
import it.reti.percorsi.pokedex.utils.LocalRepository
import it.reti.percorsi.pokedex.utils.MockRepository
import it.reti.percorsi.pokedex.utils.RemoteRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.Call

class PokemonListFragmentViewModel: ViewModel {

    //Repo refs
    private var pokemonRepository: LocalRepository
    private var remoteRepository: RemoteRepository

    //Observable vars
    var pokemonList: MutableLiveData<List<Pokemon>> = MutableLiveData()
    var pokemonTypes = MutableLiveData<Array<PokemonType>>()

    constructor(application: Activity, pokemonTypesArray: Array<PokemonType>) {
        pokemonTypes.value = pokemonTypesArray
        pokemonRepository = LocalRepository(PokemonDatabase.getDatabase(application))
        remoteRepository = RemoteRepository(PokemonApi.create())
    }

    fun getLocalPokemonList() {
        pokemonRepository.getAllPokemon()
            .subscribe(object : DisposableSubscriber<List<Pokemon>>() {
                override fun onStart() {
                    super.onStart()
                }

                override fun onComplete() {
                    //not needed
                }

                override fun onNext(t: List<Pokemon>?) {
                    pokemonList.value = t
                }

                override fun onError(t: Throwable?) {
                    //show error
                }
            })
    }
}

@Suppress("UNCHECKED_CAST")
class PokemonListViewModelFactory(private val application: Activity, private val pokemonTypes: Array<PokemonType>) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PokemonListFragmentViewModel(application, pokemonTypes) as T
    }

}
