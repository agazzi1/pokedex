package it.reti.percorsi.pokedex.synchro

import java.util.*

object RandomAlghoritm {

    val POKEMON_TOT: Int = 864
    var lastLimit: Int = 0

    init {
    }

    fun getRandomLimitAndOffset(): List<Int> {
        val limit: Int = Random().nextInt(POKEMON_TOT/10)
        var offset: Int = 0

        if (lastLimit != 0)
            offset = Random().nextInt(lastLimit)

        lastLimit = limit

        return listOf(
            limit,
            offset)
    }

}